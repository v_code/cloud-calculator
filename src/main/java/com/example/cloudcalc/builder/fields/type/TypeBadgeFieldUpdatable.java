package com.example.cloudcalc.builder.fields.type;

public interface TypeBadgeFieldUpdatable {
    void updateNameFieldPlaceholder(String placeholder);
    void updateDateFieldPlaceholder(String placeholder);
}
