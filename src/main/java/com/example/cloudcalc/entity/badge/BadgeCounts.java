package com.example.cloudcalc.entity.badge;

public class BadgeCounts {

    private int total;
    private int ignore;
    private int totalArcade;
    private int arcade;
    private int skillForArcade;
    private int skill;
    private int pdf;
    private int prizePDF;
    private int prizeSkill;
    private int prizeActivity;
    private int prizePL;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getIgnore() {
        return ignore;
    }

    public void setIgnore(int ignore) {
        this.ignore = ignore;
    }

    public int getTotalArcade() {
        return totalArcade;
    }

    public void setTotalArcade(int totalArcade) {
        this.totalArcade = totalArcade;
    }

    public int getArcade() {
        return arcade;
    }

    public void setArcade(int arcade) {
        this.arcade = arcade;
    }

    public int getSkillForArcade() {
        return skillForArcade;
    }

    public void setSkillForArcade(int skillForArcade) {
        this.skillForArcade = skillForArcade;
    }

    public int getSkill() {
        return skill;
    }

    public void setSkill(int skill) {
        this.skill = skill;
    }

    public int getPdf() {
        return pdf;
    }

    public void setPdf(int pdf) {
        this.pdf = pdf;
    }

    public int getPrizePDF() {
        return prizePDF;
    }

    public void setPrizePDF(int prizePDF) {
        this.prizePDF = prizePDF;
    }

    public int getPrizeSkill() {
        return prizeSkill;
    }

    public void setPrizeSkill(int prizeSkill) {
        this.prizeSkill = prizeSkill;
    }

    public int getPrizeActivity() {
        return prizeActivity;
    }

    public void setPrizeActivity(int prizeActivity) {
        this.prizeActivity = prizeActivity;
    }

    public int getPrizePL() {
        return prizePL;
    }

    public void setPrizePL(int prizePL) {
        this.prizePL = prizePL;
    }
}
