package com.example.cloudcalc.constant;

public interface FileName {

    String PRIZES_FILE = "prizes.json";
    String PROFILES_FILE = "profiles.json";
    String IGNORE_FILE = "ignore.json";
    String ARCADE_FILE = "arcade.json";
    String TYPES_BADGE_FILE = "types_badge.json";
    String SETTINGS_FILE = "settings.json";
    String PROGRAMS_FILE = "programs.json";

}