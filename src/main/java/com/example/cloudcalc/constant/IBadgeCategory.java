package com.example.cloudcalc.constant;

public interface IBadgeCategory {

    String TOTAL = "Total";
    String IGNORE = "Ignore";
    String TOTAL_ARCADE = "Arcade:";
    String ARCADE = "- only arcade";
    String SKILL_ARCADE = "- skills for arcade";
    String SKILL = "Skill:";
    String PDF_TOTAL = "- pdf";

    String PDF_FOR_PRIZE = "PDF for prize";
    String SKILL_FOR_PRIZE = "No PDF for prize";
    String SKILL_FOR_ACTIVITY = "Activity for prize";
    String SKILL_FOR_PL = "PL for prize";



}
